<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tables`.
 */
class m160514_195428_create_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tables', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tables');
    }
}
