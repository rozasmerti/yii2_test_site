<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\Role;

 	/* @var $this yii\web\View */
		/* @var $searchModel app\models\UserSearch */ 
		/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name:ntext',
            'phone:ntext',
            'email:ntext',
            //'roleId',
            [
    'attribute' => 'Группа',
    'value' => 'roleId',
    'content'=>function($data){
        return Role::findOne($data->roleId)->name;
    },
    'filter' => Html::activeDropDownList($searchModel, 'roleId', ArrayHelper::map(Role::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Выберите группу']),
],
            // 'password',
            // 'authKey',
            // 'accessToken',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
