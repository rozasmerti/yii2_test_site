<?php

/* @var $this yii\web\View */

$this->title = 'Мой мини-сайт';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Приветствуем вас на нашем сайте</h1>

        
    </div>

    <div class="body-content">
		<?php if(!Yii::$app->user->isGuest):?>
        <div class="row">
            <div class="col-lg">
                <h2>Добро пожаловать, <?php echo Yii::$app->user->identity->name;?>!</h2>

            </div>
            
        </div>
		<?php endif;?>
    </div>
</div>
